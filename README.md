# C9OPlayer
C9OPlayer framework is provided by Cloud9 Online LLC to play single and list of Meditations.


## Requirements
- Xcode 12.0 or later
- iOS 11.0 or later
- Swift 5.0 or later

## Installation
### Cocoapods
1. This framework is available through [CocoaPods](http://cocoapods.org). 
2. Include C9OPlayer in your `Pod File`:

```
pod 'C9OPlayer'
```

## Usage

```swift
// Initialize player with meditation and play it.

let player = C9OPlayer(meditation: C9OMeditation) // To play single meditation
OR
let player = C9OPlayer(meditation: [C9OMeditation]) // To play multiple meditations i.e. playlist etc.

player.play { (success, error) in     
}

// To pause
player.pause()

// To resume
player.resume()

// To stop
player.stop()

// To forward
player.froward()

// To backward
player.backward()

// To set repeat
player.isRepeat = true

// To set forward & backward duration
player.forwardBackwardSeconds = 5
```

## Observers
```swift
player.delegate = self
```

You can listen for player state changes by using following delegate functions:
```swift
extension ViewController: C9OPlayerDelegate {
    
    func didResume() {}
    
    func didPause() {}
    
    func didPressedForward() {}
    
    func didPressedBackward() {}
    
    func didChangeDuration(currentTime: String, remainingTime: String, sliderValue: Float64) {
        DispatchQueue.main.async {
            self.currentTimeLabel.text = currentTime
            self.totalDurationLabel.text = remainingTime
        }
    }
    
    func didEndPlaying() {}

    func errorDidOccurred(error: String) {
        print(error)
    }
    
}
```
