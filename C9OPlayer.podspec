Pod::Spec.new do |spec|

  spec.name                     = "C9OPlayer"
  spec.version                  = "0.0.2"
  spec.summary                  = "C9OPlayer framework is provided by Cloud9 Online LLC to play single and list of Meditations."
  spec.description              = "C9OPlayer contains the functionality to play media for Cloud9 Online LLC applications and perform all actions related to playing media."
  spec.homepage                 = "https://gitlab.com/c9opublicframeworks/c9oplayerframework"
  spec.license                  = { :type => 'MIT', :text => <<-LICENSE
                                        Copyright © 2021 Cloud9 Online LLC.
                                    LICENSE
                                  }
  spec.author                   = { "Cloud9 Online" => "cloud9online@gmail.com" }
  spec.ios.deployment_target    = "11.0"
  spec.source                   = { :git => "https://gitlab.com/c9opublicframeworks/c9oplayerframework.git", :tag => "#{spec.version}" }
  spec.vendored_frameworks      = "C9OPlayer.framework"
  spec.swift_versions           = ['5.0']
  
  spec.dependency                 "C9OCore"
  spec.dependency                 "C9OMediaManager"
  spec.weak_frameworks          = 'Foundation', 'SystemConfiguration'
  
  spec.user_target_xcconfig     = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }

end
